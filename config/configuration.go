package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

// Configuration contains application settings
type Configuration struct {
	ConnectionString      string `json:"connection_string"`
	ConnectionStringLocal string `json:"connection_string_local"`
}

// GetConfiguration return Configuration object
func GetConfiguration() Configuration {
	// read configuration
	var config Configuration
	file, err := ioutil.ReadFile("./configuration.json")
	if err != nil {
		fmt.Printf("File error: %v\n", err)
		os.Exit(1)
	}
	json.Unmarshal(file, &config)
	return config
}
