package config

import (
	"milan_rapic/utils"
	"net/http"

	"github.com/gorilla/sessions"
)

var sessionStore *sessions.CookieStore

// CreateSessionStore will create new session
func CreateSessionStore() {
	sessionStore = sessions.NewCookieStore([]byte(utils.RandomString(32)))
}

// GetSession returns session object
func GetSession(r *http.Request) *sessions.Session {
	session, err := sessionStore.Get(r, "auth-session")
	if err != nil {
		//log.Println("Error - Cant get session from session store: " + err.Error())
	}

	/*
		if session.IsNew {
			// Set some cookie options
			session.Options.MaxAge = 3600 * 1 // 1 hour
		}
	*/

	return session
}
