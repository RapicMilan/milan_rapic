package config

import (
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

// Connect will open database session and test if it works
func Connect(connectionString string) {
	var err error
	db, err = sql.Open("mysql", connectionString)
	if err != nil {
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	log.Println("Connected to Database")
}

// Close will close database session
func Close() {
	err := db.Close()
	if err != nil {
		log.Println(err)
	}

	log.Println("Closed database connection")
}

// GetDb returns *sql.DB object
func GetDb() *sql.DB {
	return db
}
