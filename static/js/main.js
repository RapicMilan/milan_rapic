$(function(){

    init();

    $('#logout-btn').on('click', function (e) {
        e.preventDefault();
        signOut();
        $.ajax({
            url: '/api/logout',
            type: 'post',
            success: function(response){
                if (response == "success") {
                    window.location = '/login';
                }                          
            }
        });
    });

    function init() {
        $.ajax({
            url: '/api/validate-session',
            type: 'post',
            success: function(response){
                if (response == "success") {
                    // User is logged in, show logout                     
                    $('#logout-btn').addClass("show")
                } else {
                    // User is not logged in, hide logout
                    $('#logout-btn').removeClass("show")
                }                
            }
        });
    }
});


// Google API

function onLoad() {
    gapi.load('auth2', function() {
        gapi.auth2.init();
    });
}

function signOut() {
    gapi.auth2.init();
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
};

function onSignIn(googleUser) {
    // The ID token you need to pass to your backend:
    var profile = googleUser.getBasicProfile();
    var id_token = googleUser.getAuthResponse().id_token;
        
    var auth = { 
        auth_type: "google",
        email: profile.getEmail()
    }
        
    // Send Token to server
    $.ajax({
        url: '/api/login',
        type: 'post',
        data: auth
    })
    .done(function(response) {
        if (response == "success") {
            window.location = '/profile';
        }
    });
};



// Email validation
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};