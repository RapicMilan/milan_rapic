(function(){
    $(function(){

        var formValid = {
            email: false,
            password: false,
            password2: false
        };

        $('form').on('submit', function (e) {
            e.preventDefault();

            // Check if form is valid

            var valid = formValid.email && formValid.password && formValid.password2;
            if (valid) {
                // Form is valid
            
                $.ajax({
                    url: '/api/register',
                    type: 'post',
                    data: $('form').serialize(),
                })
                .done(function(response) {
                    if (response == 'success') {
                        window.location = '/profile';
                    } else {
                        $('form')[0].reset();
                    }
                });
            
            } else {
                var message = "Form is not valid:\n\n";
                if (!formValid.email) { message += "Please enter valid email.\n"; }
                if (!formValid.password) { message += "Please enter password.\n"; }
                if (!formValid.password2) { message += "Password does not match.\n"; }
                alert(message);
            }
        });

        $('#email').keyup(function()
        {
            var valid = isEmail($(this).val());

            if (!valid) {
                //Email is not valid
                $("#email-input").removeClass("has-success")
                $("#email-input").addClass("has-error has-feedback")
                $("#email-input .glyphicon").removeClass("glyphicon-ok")
                formValid.email = false;

            } else {
                $("#email-input").removeClass("has-error")
                $("#email-input").addClass("has-success has-feedback")
                $("#email-input .glyphicon").addClass("glyphicon-ok")

                formValid.email = true;
            }
        });

        $('#password').keyup(function()
        {
           if( !$(this).val() ) {
                $("#password-input").removeClass("has-success")
                $("#password-input").addClass("has-error has-feedback")
                $("#email-input .glyphicon").removeClass("glyphicon-ok")
                formValid.password = false;

            } else {
                $("#password-input").removeClass("has-error")
                $("#password-input").addClass("has-success has-feedback")
                $("#email-input .glyphicon").addClass("glyphicon-ok")
                formValid.password = true;


                if($('#password').val() != $('#password2').val()) {
                    //Password is not the same
                    $("#password-input").removeClass("has-success")
                    $("#password-input").addClass("has-error has-feedback")
                    $("#password-input .glyphicon").removeClass("glyphicon-ok")

                    $("#password2-input").removeClass("has-success")
                    $("#password2-input").addClass("has-error has-feedback")
                    $("#password2-input .glyphicon").removeClass("glyphicon-ok")
                    
                    formValid.password2 = false;

                } else {
                    $("#password-input").removeClass("has-error")
                    $("#password-input").addClass("has-success has-feedback")
                    $("#password-input .glyphicon").addClass("glyphicon-ok")

                    $("#password2-input").removeClass("has-error")
                    $("#password2-input").addClass("has-success has-feedback")
                    $("#password2-input .glyphicon").addClass("glyphicon-ok")
                    formValid.password2 = true;
                }
            }
        });

        $('#password2').keyup(function()
        {
           if($('#password').val() != $('#password2').val()) {
                //Password is not the same
                $("#password-input").removeClass("has-success")
                $("#password-input").addClass("has-error has-feedback")
                $("#password-input .glyphicon").removeClass("glyphicon-ok")

                $("#password2-input").removeClass("has-success")
                $("#password2-input").addClass("has-error has-feedback")
                $("#password2-input .glyphicon").removeClass("glyphicon-ok")
                
                formValid.password2 = false;

            } else {
                $("#password-input").removeClass("has-error")
                $("#password-input").addClass("has-success has-feedback")
                $("#password-input .glyphicon").addClass("glyphicon-ok")

                $("#password2-input").removeClass("has-error")
                $("#password2-input").addClass("has-success has-feedback")
                $("#password2-input .glyphicon").addClass("glyphicon-ok")
                formValid.password2 = true;
            }
        });


    });

})();