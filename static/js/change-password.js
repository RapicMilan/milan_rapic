(function(){
    $(function(){

        var formValid = {
            password: false,
            password2: false
        };

        $('form').on('submit', function (e) {
            e.preventDefault();

            // Check if form is valid
            var valid = formValid.password && formValid.password2;
            if (valid) {
                // Form is valid
            
                var token = getUrlParameter('token');
                var data = {
                    token: token,
                    password: $("#password").val()
                }
            
                $.ajax({
                    url: '/api/change-password',
                    type: 'post',
                    data: data,
                })
                .done(function(response) {
                    if (response == 'success') {
                        window.location = '/profile';
                    } else {
                        $('form')[0].reset();
                    }
                });
            
            } else {
                var message = "Form is not valid:\n\n";
                if (!formValid.password) { message += "Please enter password.\n"; }
                if (!formValid.password2) { message += "Password does not match.\n"; }
                alert(message);
            }
        });

        $('#password').keyup(function()
        {
           if( !$(this).val() ) {
                console.log("Password is empty")
                $("#password-input").removeClass("has-success")
                $("#password-input").addClass("has-error has-feedback")
                formValid.password = false;

            } else {
                $("#password-input").removeClass("has-error")
                $("#password-input").addClass("has-success has-feedback")
                formValid.password = true;


                if($('#password').val() != $('#password2').val()) {
                    console.log("Password is not the same")
                    $("#password-input").removeClass("has-success")
                    $("#password-input").addClass("has-error has-feedback")
                    $("#password-input .glyphicon").removeClass("glyphicon-ok")

                    $("#password2-input").removeClass("has-success")
                    $("#password2-input").addClass("has-error has-feedback")
                    $("#password2-input .glyphicon").removeClass("glyphicon-ok")
                    
                    formValid.password2 = false;

                } else {
                    $("#password-input").removeClass("has-error")
                    $("#password-input").addClass("has-success has-feedback")
                    $("#password-input .glyphicon").addClass("glyphicon-ok")

                    $("#password2-input").removeClass("has-error")
                    $("#password2-input").addClass("has-success has-feedback")
                    $("#password2-input .glyphicon").addClass("glyphicon-ok")
                    formValid.password2 = true;
                }
            }
        });

        $('#password2').keyup(function()
        {
           if($('#password').val() != $('#password2').val()) {
                console.log("Password is not the same")
                $("#password-input").removeClass("has-success")
                $("#password-input").addClass("has-error has-feedback")
                $("#password-input .glyphicon").removeClass("glyphicon-ok")

                $("#password2-input").removeClass("has-success")
                $("#password2-input").addClass("has-error has-feedback")
                $("#password2-input .glyphicon").removeClass("glyphicon-ok")
                
                formValid.password2 = false;

            } else {
                $("#password-input").removeClass("has-error")
                $("#password-input").addClass("has-success has-feedback")
                $("#password-input .glyphicon").addClass("glyphicon-ok")

                $("#password2-input").removeClass("has-error")
                $("#password2-input").addClass("has-success has-feedback")
                $("#password2-input .glyphicon").addClass("glyphicon-ok")
                formValid.password2 = true;
            }
        });


    });

})();