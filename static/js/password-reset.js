(function(){
    $(function(){

        init()

        function init() {
            $('.email-sent-message').hide();
            $('.email-error-message').hide();
        }

        var formValid = {
            email: false,
            password: false,
            password2: false
        };

        $('form').on('submit', function (e) {
            e.preventDefault();

            // Check if form is valid

            var valid = formValid.email;
            if (valid) {
                // Form is valid
            
                $.ajax({
                    url: '/api/password-reset',
                    type: 'post',
                    data: $('form').serialize(),
                })
                .done(function(response) {
                    if (response == 'success') {
                        // Show success message
                        $('.email-sent-message').show();
                        $('.email-error-message').hide();
                        $('form').hide()
                    } else {
                        // Show error message
                        $('.email-sent-message').hide();
                        $('.email-error-message').show();
                        $('form').hide()
                    }
                });
            
            } else {
                var message = "Form is not valid:\n\n";
                if (!formValid.email) { message += "Please enter valid email.\n"; }
                if (!formValid.password) { message += "Please enter password.\n"; }
                if (!formValid.password2) { message += "Password does not match.\n"; }
                alert(message);
            }
        });

        $('#show-form').click(function(e){
            e.preventDefault();
            $('.email-sent-message').hide();
            $('.email-error-message').hide();
            $('form').show()
        });

        $('#email').keyup(function()
        {
            var valid = isEmail($(this).val());

            if (!valid) {
                console.log("Email is not valid")
                $("#email-input").removeClass("has-success")
                $("#email-input").addClass("has-error has-feedback")
                $("#email-input .glyphicon").removeClass("glyphicon-ok")
                formValid.email = false;

            } else {
                $("#email-input").removeClass("has-error")
                $("#email-input").addClass("has-success has-feedback")
                $("#email-input .glyphicon").addClass("glyphicon-ok")

                formValid.email = true;
            }
        });


    });

})();