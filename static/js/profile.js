(function(){
    $(function(){

        init();

        var formValid = {
            email: true
        };

        function init() {
            $('#cancel-edit').hide();
            $('#save-profile').hide();

            $.ajax({
                url: '/api/profile',
                type: 'json',
                type: 'get',
                success: function(response){
                    $('#full_name').val(response.full_name);
                    $('#email').val(response.email);
                    $('#address').val(response.address);
                    $('#telephone').val(response.telephone);      
                }
            });
        }

        $('#email').keyup(function()
        {
            var valid = isEmail($(this).val());
            if (!valid) {
                $("#email-input").removeClass("has-success")
                $("#email-input").addClass("has-error has-feedback")
                $("#email-input .glyphicon").removeClass("glyphicon-ok")
                formValid.email = false;

            } else {
                $("#email-input").removeClass("has-error")
                $("#email-input").addClass("has-success has-feedback")
                $("#email-input .glyphicon").addClass("glyphicon-ok")

                formValid.email = true;
            }
        });

        $('#edit-profile').click(function(e){
            e.preventDefault();

            $('#full_name').removeAttr("disabled");
            $('#email').removeAttr("disabled");
            $('#address').removeAttr("disabled");
            $('#telephone').removeAttr("disabled");
            $('#save-profile').removeAttr("disabled");
            $('#edit-profile').attr('disabled', 'disabled');
            $('#save-profile').show();
            $('#cancel-edit').show();
        });

        $('#cancel-edit').click(function(e){
            e.preventDefault();

            $('#full_name').attr('disabled', 'disabled');
            $('#email').attr('disabled', 'disabled');
            $('#address').attr('disabled', 'disabled');
            $('#telephone').attr('disabled', 'disabled');
            $('#save-profile').attr('disabled', 'disabled');
            $('#edit-profile').removeAttr("disabled");
            $('#save-profile').hide();
            $('#cancel-edit').hide();
        });


         $('form').on('submit', function (e) {
            e.preventDefault();

            // Check if form is valid

            var valid = formValid.email;
            if (valid) {
                // Form is valid
            
                $.ajax({
                    url: '/api/profile',
                    type: 'post',
                    data: $('form').serialize(),
                })
                .done(function(response) {
                    if (response == 'success') {
                        window.location = '/profile';
                    }
                });
            
            } else {
                var message = "Form is not valid:\n\n";
                if (!formValid.email) { message += "Please enter valid email.\n"; }
                alert(message);
            }
        });

    });
})();





