(function(){
    $(function(){

        $('form').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: '/api/login',
                type: 'post',
                data: $('form').serialize()
            })
            .done(function(response) {
                if (response == 'success') {
                    window.location = '/profile';
                } else {
                    $('form')[0].reset();
                }
            });
        });     
    });
})();