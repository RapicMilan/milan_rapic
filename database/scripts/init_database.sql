CREATE DATABASE IF NOT EXISTS auth_test;
USE auth_test

-- Create table
CREATE TABLE IF NOT EXISTS user_info (
    uid        INT(10) NOT NULL AUTO_INCREMENT,
    email      VARCHAR(64) NOT NULL UNIQUE,
    password   VARCHAR(64) NOT NULL,
    address    VARCHAR(250) NULL DEFAULT NULL,
    telephone  VARCHAR(64) NULL DEFAULT NULL,
    full_name   VARCHAR(64) NULL DEFAULT NULL,
    login_type VARCHAR(64) NULL DEFAULT NULL,
    created    DATETIME DEFAULT   CURRENT_TIMESTAMP,
    modified   DATETIME ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (uid)
);

CREATE TABLE IF NOT EXISTS password_change_requests (
    uid             INT(10) NOT NULL AUTO_INCREMENT,
    email           VARCHAR(64) NOT NULL,
    token           VARCHAR(250) NOT NULL,
    date_generated  DATETIME DEFAULT CURRENT_TIMESTAMP,
    date_expires    DATETIME NULL DEFAULT NULL,
    date_used       DATETIME NULL DEFAULT NULL,
    PRIMARY KEY (uid)
);

CREATE TRIGGER before_insert_on_password_change_requests BEFORE INSERT ON `password_change_requests` 
FOR EACH ROW SET new.date_expires = IFNULL(new.date_expires,DATE_ADD(NOW(), INTERVAL 1 DAY));



-- Insert admin user
-- INSERT INTO user_info (email, password, address, telephone, full_name)
-- VALUES ('admin@gmail.com', '$2a$10$Yg9TNBG7FxCOQ4iqiKffcuzadiwtH32Pbsfu9I57YkYRDPOO.HlvO', 'Dositeja Obradovica 83, Rumenka', '+381 69 220-9374', 'Milan Rapic');





-- mysql -u root -p < database/scripts/init_database.sql