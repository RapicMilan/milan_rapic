package entities

import (
	"time"
)

// PasswordChangeRequest is used when user request password reset
type PasswordChangeRequest struct {
	Email         string
	Token         string
	DateGenerated time.Time
	DateExpires   time.Time
	DateUsed      time.Time
}
