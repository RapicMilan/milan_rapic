package entities

import (
	"golang.org/x/crypto/bcrypt"
)

// User struct represents User entity
type User struct {
	Email     string `json:"email"`
	Password  []byte `json:"password"`
	Address   string `json:"address"`
	Telephone string `json:"telephone"`
	FullName  string `json:"full_name"`
	LoginType string `json:"-"`
}

// SetPassword takes a plaintext and hashes it with bcrypt and sets the password field with the hash
func (u *User) SetPassword(password string) {
	hpassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		// This is a panic because bcrypt errors on invalid costs
		panic(err)
	}
	u.Password = hpassword
}
