package repository

import (
	"log"
	"milan_rapic/config"
	"milan_rapic/database/entities"
	"milan_rapic/utils"
	"time"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

type passwordChangeRepository struct {
}

// PasswordChangeRepository returns pointer to passwordChangeRepository struct
func PasswordChangeRepository() *passwordChangeRepository {
	return &passwordChangeRepository{}
}

func (p *passwordChangeRepository) ChangePassword(nu *entities.User) error {
	db := config.GetDb()

	// Prepare SQL statement
	st, err := db.Prepare("UPDATE user_info SET password=? WHERE email=?")
	if err != nil {
		log.Println("Error, cannto prepare SQL statement: ", err.Error())
		return err
	}
	defer st.Close()

	// Update row
	_, err = st.Exec(nu.Password, nu.Email)
	if err != nil {
		log.Println("Error, cannot update password: ", err.Error())
		return err
	}
	return nil
}

func (p *passwordChangeRepository) PasswordReset(email string) (string, error) {
	db := config.GetDb()

	// Generate random string
	token, err := utils.GenerateRandomString(32)
	if err != nil {
		log.Println("Error, cannot generate token: ", err.Error())
		return "", err
	}

	// Prepare SQL statement
	st, err := db.Prepare("INSERT INTO password_change_requests (email, token) VALUES (?, ?)")
	if err != nil {
		log.Println("Error, cannot prepare SQL statement: ", err.Error())
		return "", err
	}
	defer st.Close()

	// Update row
	_, err = st.Exec(email, token)
	if err != nil {
		return "", err
	}

	// Use sendgrid to send email with password reset link
	from := mail.NewEmail("Authentication service", "rapic.milan@gmail.com")
	subject := "Password reset"
	to := mail.NewEmail("User", email)
	plainTextContent := "To reset your password please visit this link: http://powerprototype.com/change-password?token=" + token
	htmlContent := "<strong>To reset your password please visit this link:</strong> http://powerprototype.com/change-password?token=" + token
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)
	client := sendgrid.NewSendClient("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")
	_, err = client.Send(message)
	if err != nil {
		log.Println("Error, cannot send email: ", err.Error())
		return "", err
	}

	return token, nil
}

// ValidateToken checks if token exist in database and check if it is expired
func (p *passwordChangeRepository) ValidateToken(token string) bool {
	db := config.GetDb()
	pcr := entities.PasswordChangeRequest{}
	valid := false

	// Prepare SQL statement
	st, err := db.Prepare("SELECT email, token, date_generated, date_expires FROM password_change_requests WHERE token=?")
	if err != nil {
		log.Println("Error: cannot create SQL statement, ", err.Error())
		return false
	}
	defer st.Close()

	// Fetch row
	err = st.QueryRow(token).Scan(&pcr.Email, &pcr.Token, &pcr.DateGenerated, &pcr.DateExpires)
	if err != nil {
		log.Println("Error: cannot get password change requests from database, ", err.Error())
		return false
	}

	now := time.Now()
	diff := now.Sub(pcr.DateExpires)

	// negative number, if 23h is left until expire, then ramaining = -23
	remaining := int(diff.Hours())
	if remaining <= 0 {
		// Token is still valid
		valid = true

	} else {
		// Token expired
		valid = false
	}

	return valid
}

// ProccessPasswordChangeRequest will update password change request record
// and mark it as finished, and email is returned
func (p *passwordChangeRepository) ProccessPasswordChangeRequest(token string) string {
	db := config.GetDb()

	// Prepare SQL statement
	st, err := db.Prepare("SELECT email FROM password_change_requests WHERE token=?")
	if err != nil {
		log.Println("Error: cannot create SQL statement, ", err.Error())
		return ""
	}
	defer st.Close()

	var email string

	// Fetch row
	err = st.QueryRow(token).Scan(&email)
	if err != nil {
		log.Println("Error: cannot read email from database, ", err.Error())
		return ""
	}

	// Prepare SQL statement
	stu, err := db.Prepare("UPDATE password_change_requests SET date_used=? WHERE token=?")
	if err != nil {
		log.Println("Error, cannot create SQL statement, ", err.Error())
		return ""
	}
	defer stu.Close()

	// Update row
	_, err = stu.Exec(time.Now(), token)
	if err != nil {
		log.Println("Error, cannot update date_used, ", err.Error())
		return ""
	}

	return email
}
