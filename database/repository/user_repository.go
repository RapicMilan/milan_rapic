package repository

import (
	"database/sql"
	"log"
	"milan_rapic/config"
	"milan_rapic/database/entities"

	"golang.org/x/crypto/bcrypt"
)

type userRepository struct {
}

// UserRepository returns pointer to userRepository struct
func UserRepository() *userRepository {
	return &userRepository{}
}

// GetUser will search user_info for provided email
func (u *userRepository) GetUser(email string) (*entities.User, error) {
	db := config.GetDb()
	user := entities.User{}

	// Prepare SQL statement
	st, err := db.Prepare("SELECT email, password, address, telephone, full_name, login_type FROM user_info WHERE email=?")
	if err != nil {
		log.Println("Error: cannot prepare SQL statement: ", err.Error())
		return nil, err
	}
	defer st.Close()

	// Handle NULL values
	var nEmail sql.NullString
	var nPassword sql.NullString
	var nAddress sql.NullString
	var nTelephone sql.NullString
	var nFullName sql.NullString
	var nLoginType sql.NullString

	// Fetch row
	err = st.QueryRow(email).Scan(&nEmail, &nPassword, &nAddress, &nTelephone, &nFullName, &nLoginType)
	if err != nil {
		log.Println("Error: cannot read from database: ", err.Error())
		return nil, err
	}

	if nEmail.Valid {
		user.Email = nEmail.String
	}
	if nPassword.Valid {
		user.Password = []byte(nPassword.String)
	}
	if nAddress.Valid {
		user.Address = nAddress.String
	}
	if nTelephone.Valid {
		user.Telephone = nTelephone.String
	}
	if nFullName.Valid {
		user.FullName = nFullName.String
	}

	if nLoginType.Valid {
		user.LoginType = nLoginType.String
	}

	// If user does not exists
	if err != nil {
		return nil, err
	}

	// User exists
	return &user, nil

}

// Login validates and returns a User object if they exists in the database
func (u *userRepository) Login(email, password string) (*entities.User, error) {

	// If user does not exists, return value is nil
	user, err := u.GetUser(email)

	// If user exists
	if user != nil {
		// Compare passwords
		err = bcrypt.CompareHashAndPassword(user.Password, []byte(password))
		if err != nil {
			// If password comparer return error than password is incorect
			// user is return as nil, authentication failed
			log.Println("Error: Authentication failed for user: " + user.Email + " with password: " + password + "!")
			user = nil
		}
	}

	// If authentication is successfull user is return, if authentication failed user is nil
	return user, err
}

// Register will add new user_info record to database
func (u *userRepository) Register(nu *entities.User) error {
	db := config.GetDb()

	// Prepare SQL statement
	st, err := db.Prepare("INSERT INTO user_info (email, password, login_type) values (?, ?, ?)")
	if err != nil {
		log.Println("Error: cannot prepare SQL statement: ", err.Error())
		return err
	}
	defer st.Close()

	// Insert row
	_, err = st.Exec(nu.Email, nu.Password, nu.LoginType)
	if err != nil {
		log.Println("Error: cannot insert record to database: ", err.Error())
		return err
	}

	//log.Println("User: " + nu.Email + " with password: " + string(nu.Password) + " is added to database!")

	return nil
}

// UpdateProfile will update user_info record for provided email
func (u *userRepository) UpdateProfile(nu *entities.User) error {
	db := config.GetDb()

	// Prepare SQL statement
	st, err := db.Prepare("UPDATE user_info SET email=?, address=?, telephone=?, full_name=? WHERE email=?")
	if err != nil {
		log.Println("Error: cannot prepare SQL statement: ", err.Error())
		return err
	}
	defer st.Close()

	// Update row
	_, err = st.Exec(nu.Email, nu.Address, nu.Telephone, nu.FullName, nu.Email)
	if err != nil {
		log.Println("Error: cannot update database record: ", err.Error())
		return err
	}

	// If adding user is successfull, nil is returned, otherwise error is returned
	return nil
}
