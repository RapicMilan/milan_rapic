# Authentication test project #



### Dependencies ###

* github.com/julienschmidt/httprouter
* golang.org/x/crypto/bcrypt
* github.com/sendgrid/sendgrid-go
* github.com/sendgrid/sendgrid-go/helpers/mail
* github.com/gorilla/sessions

### Project setup ###
This project uses Google Compute Engine for hosting, SendGrid and Postfix were used for sending emails 

For more information about this setup check this link: [https://cloud.google.com/compute/docs/tutorials/sending-mail/using-sendgrid](https://cloud.google.com/compute/docs/tutorials/sending-mail/using-sendgrid)


