package routing

import (
	"milan_rapic/handlers/authentication"

	"milan_rapic/handlers/frontend"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// Init will initialize routing
func Init() {
	router := httprouter.New()

	// Frontend routing
	router.GET("/", frontend.HomePage)
	router.GET("/profile", authentication.Authenticate(frontend.ProfilePage))
	router.GET("/login", frontend.LoginPage)
	router.GET("/register", frontend.RegisterPage)

	router.GET("/password-reset", frontend.PasswordResetPage)              // Enter email, send email with token
	router.GET("/change-password", frontend.ChangePasswordPage)            // Validate token, render page, User enters new password, and submit token and password
	router.GET("/password-reset-failed", frontend.PasswordResetFailedPage) // Render if token is invalid

	router.GET("/frontend", frontend.FrontendTestPage)

	// REST API routing
	router.POST("/api/login", authentication.Login)
	router.POST("/api/logout", authentication.Logout)
	router.POST("/api/register", authentication.Register)

	router.POST("/api/password-reset", authentication.PasswordReset) // Request password change, send email
	router.POST("/api/change-password", authentication.ChangePassword)
	router.POST("/api/validate-session", authentication.ValidateSession)

	router.GET("/api/profile", authentication.Authenticate(authentication.GetProfile))
	router.POST("/api/profile", authentication.Authenticate(authentication.UpdateProfile))

	// Serve static files
	router.ServeFiles("/static/*filepath", http.Dir("static"))

	http.Handle("/", router)
}
