package main

import (
	"log"
	"milan_rapic/config"
	"milan_rapic/routing"
	"net/http"
)

func main() {

	// Get configuration
	configuration := config.GetConfiguration()

	// Open connection to database
	config.Connect(configuration.ConnectionString)
	defer config.Close()

	// Initialize session
	config.CreateSessionStore()

	// Initialize routing
	routing.Init()

	// Start web server
	log.Println("Server is listening on: http://localhost:8000")
	err := http.ListenAndServe(":8000", nil)
	if err != nil {
		log.Println("Error starting server!")
		panic(err)
	}
}
