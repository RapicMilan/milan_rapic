package frontend

import (
	"html/template"
	"milan_rapic/database/repository"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

/** ************************************************************************ **
 ** Home page
 ** ************************************************************************ **/
var homeTemplate = template.Must(template.ParseFiles(
	"templates/_base.html",
	"templates/home.html",
))

// HomePage handler redirect to profile page
func HomePage(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// redirect to profile page
	http.Redirect(w, r, "/profile", http.StatusFound)
}

/** ************************************************************************ **
 ** Profile page
 ** ************************************************************************ **/
var profileTemplate = template.Must(template.ParseFiles(
	"templates/_base.html",
	"templates/profile.html",
))

// ProfilePage handler renders profile page
func ProfilePage(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	err := profileTemplate.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
}

/** ************************************************************************ **
 ** Login page
 ** ************************************************************************ **/
var loginTemplate = template.Must(template.ParseFiles(
	"templates/_base.html",
	"templates/login.html",
))

// LoginPage handler renders login page
func LoginPage(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	err := loginTemplate.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
}

/** ************************************************************************ **
 ** Register page
 ** ************************************************************************ **/
var registerTemplate = template.Must(template.ParseFiles(
	"templates/_base.html",
	"templates/register.html",
))

// RegisterPage handler renders register page
func RegisterPage(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	err := registerTemplate.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
}

/** ************************************************************************ **
 ** Password reset page
 ** ************************************************************************ **/
var passwordResetTemplate = template.Must(template.ParseFiles(
	"templates/_base.html",
	"templates/password-reset.html",
))

// PasswordResetPage handler renders password reset page
func PasswordResetPage(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	err := passwordResetTemplate.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
}

/** ************************************************************************ **
 ** Password reset failed page
 ** ************************************************************************ **/
var passwordResetFailedTemplate = template.Must(template.ParseFiles(
	"templates/_base.html",
	"templates/password-reset-failed.html",
))

// PasswordResetFailedPage handler renders password reset failed page
func PasswordResetFailedPage(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	err := passwordResetFailedTemplate.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
}

/** ************************************************************************ **
 ** Change password page
 ** ************************************************************************ **/
var changePasswordTemplate = template.Must(template.ParseFiles(
	"templates/_base.html",
	"templates/change-password.html",
))

// ChangePasswordPage handler renders password reset page
func ChangePasswordPage(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	r.ParseForm()
	token := r.FormValue("token")

	// Check token, if error redirect to PasswordResetFailedPage
	valid := repository.PasswordChangeRepository().ValidateToken(token)
	if !valid {
		http.Redirect(w, r, "/password-reset-failed", http.StatusFound)
		return
	}

	// If token is correct, render change password page

	err := changePasswordTemplate.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
}

/** ************************************************************************ **
 ** Frontend page
 ** ************************************************************************ **/
var frontendTemplate = template.Must(template.ParseFiles(
	"templates/frontend.html",
))

// FrontendTestPage handler renders frontend page
func FrontendTestPage(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	err := frontendTemplate.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
}
