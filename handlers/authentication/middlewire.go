package authentication

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// Authenticate middleware check if user is authenticated
// if not user is redirected to login page
func Authenticate(h httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

		valid := Validate(r)
		if valid {
			// Set headers to prevent browser goBack action to restricted page after logout
			w.Header().Set("Cache-Control", "no-cache, max-age=0, no-store, must-revalidate")
			w.Header().Set("Pragma", "no-cache")
			w.Header().Set("Expires", "0")
			h(w, r, ps)
		} else {
			// Redirect to login
			http.Redirect(w, r, "/login", http.StatusFound)
		}
	}
}
