package authentication

import (
	"encoding/json"
	"log"
	"milan_rapic/config"
	"milan_rapic/database/entities"
	"milan_rapic/database/repository"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// Login handler will authenticate user, and return "success" message
// if authentication is successfull
func Login(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	r.ParseForm()
	status := "success"
	authType := r.FormValue("auth_type")
	session := config.GetSession(r)

	if authType == "google" {
		// Google login
		email := r.FormValue("email")
		session.Values["email"] = email

		err := session.Save(r, w)
		if err != nil {
			log.Println("Error: cannot save Google session: ", err.Error())
			status = "error"
		} else {
			// If this is first login, register user with email
			user, err := repository.UserRepository().GetUser(email)
			if user == nil {
				// register user
				nuser := entities.User{}
				nuser.Email = email
				nuser.Password = []byte("")
				nuser.LoginType = "google"

				err = repository.UserRepository().Register(&nuser)
				if err != nil {
					log.Println("Error: cannot register user: ", err.Error())
					status = "error"
				} else {
					// User registered successfully, now save email to session
					session := config.GetSession(r)
					session.Values["email"] = email
					err := session.Save(r, w)
					if err != nil {
						log.Println("Error: cannot save Google session: ", err.Error())
						status = "error"
					} else {
						// Session updated successfull
						status = "success"
					}
				}
			}
		}
	} else {
		// Custom login
		email := r.FormValue("email")
		password := r.FormValue("password")

		if email != "" && password != "" {
			// log in user
			user, err := repository.UserRepository().Login(email, password)
			if err != nil {
				log.Println("Error: cannot log in user: ", err.Error())
				status = "error"
			}
			// Check if user exists in database
			if user != nil {
				// If user exists in database
				// Save user to session
				session := config.GetSession(r)
				session.Values["email"] = user.Email

				err = session.Save(r, w)
				if err != nil {
					log.Println("Error, cannot save session: ", err.Error())
					status = "error"
				} else {
					// Session updated successfull
					status = "success"
				}
			}
		}
	}

	// Send response
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(status))
}

// Logout handler will log out user
func Logout(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	//Get session
	session := config.GetSession(r)

	// Delete user from session
	session.Values["email"] = ""
	session.Options.MaxAge = 0

	err := session.Save(r, w)
	if err != nil {
		log.Println("Error, cannot save session: ", err.Error())
	}

	// Send response
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("success"))
}

// Register handler will register new user
func Register(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	r.ParseForm()
	status := "error"
	email := r.FormValue("email")
	password := r.FormValue("password")

	if email != "" && password != "" {
		user := entities.User{}
		user.Email = email
		user.SetPassword(password)

		err := repository.UserRepository().Register(&user)
		if err != nil {
			log.Println("Error, cannot register user: ", err.Error())
		} else {
			// User registered successfully, now save email to session
			session := config.GetSession(r)
			session.Values["email"] = email
			err := session.Save(r, w)
			if err != nil {
				log.Println("Error, cannot save session: ", err.Error())
			} else {
				// Session updated successfull
				status = "success"
			}
		}
	}

	// Send response
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(status))
}

// UpdateProfile will update user_info record
func UpdateProfile(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	r.ParseForm()
	status := "error"
	email := r.FormValue("email")
	address := r.FormValue("address")
	telephone := r.FormValue("telephone")
	fullname := r.FormValue("full_name")

	if email != "" {
		user := entities.User{}
		user.Email = email
		user.Address = address
		user.Telephone = telephone
		user.FullName = fullname

		err := repository.UserRepository().UpdateProfile(&user)
		if err != nil {
			log.Println("Error: cannot update user profile: ", err.Error())
		} else {
			// User updated successfully, now save email to session
			session := config.GetSession(r)
			session.Values["email"] = email
			err := session.Save(r, w)
			if err != nil {
				log.Println("Error, cannot save session: ", err.Error())
			} else {
				// Session updated successfull
				status = "success"
			}
		}
	}

	// Send response
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(status))
}

// Validate returns email as string, or empty string if email does not exist in session
func Validate(r *http.Request) bool {
	valid := false
	session := config.GetSession(r)
	// Check if session is created
	if session != nil {
		// If email does not exists in session than interface is nil
		if session.Values["email"] != nil {
			if session.Values["email"] != "" {
				valid = true
			}
		}
	}
	return valid
}

// GetUserEmail returns user email, or empty string if email does not exist in session
func GetUserEmail(r *http.Request) string {
	session := config.GetSession(r)
	username := ""
	// Check if session is created
	if session != nil {
		// If email does not exists in session than interface is nil
		if session.Values["email"] != nil {
			username = session.Values["email"].(string)
		}
	}

	return username
}

// ValidateSession will check if session is valid
func ValidateSession(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	valid := Validate(r)
	response := "error"
	if valid {
		// Session is valid
		response = "success"
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(response))
}

// GetProfile return User JSON response
func GetProfile(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	email := GetUserEmail(r)
	user, err := repository.UserRepository().GetUser(email)
	if err != nil {
		log.Println("Error: cannot find profile: ", err.Error())
	}

	// clear the password
	user.Password = []byte("")

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		log.Println("Error: cannot write JSON response: ", err.Error())
	}
}

// PasswordReset will accept email,
// check if email exist in database then it will send confirmation email
func PasswordReset(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	status := "error"

	r.ParseForm()
	email := r.FormValue("email")

	if email != "" {
		user, err := repository.UserRepository().GetUser(email)
		if err != nil {
			log.Println("Error: cannot read user from database: ", err.Error())
		} else {
			if user != nil {
				if user.LoginType != "google" {
					token, err := repository.PasswordChangeRepository().PasswordReset(email)
					if err != nil {
						log.Println("Error, cannot send email with password reset link: ", err.Error())
						status = "error"
					}
					if err == nil && token != "" {
						// Email is sent, token is returned
						status = "success"
					}
				} else {
					// Cannot change email registered with Google Login
					status = "error"
				}

			}
		}
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(status))
}

// ChangePassword will accept token, search database for token, check if it is expired
// if not password will be reset but first user need to enter new password
func ChangePassword(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	status := "error"

	r.ParseForm()
	token := r.FormValue("token")
	password := r.FormValue("password")

	var email string
	email = repository.PasswordChangeRepository().ProccessPasswordChangeRequest(token)
	if email != "" {
		user := entities.User{}
		user.Email = email
		user.SetPassword(password)

		// Update password
		err := repository.PasswordChangeRepository().ChangePassword(&user)
		if err != nil {
			log.Println("Error, cannot change password: ", err.Error())
		}

		// Add user to session
		session := config.GetSession(r)
		session.Values["email"] = email
		err = session.Save(r, w)
		if err != nil {
			log.Println("Error, cannot save session: ", err.Error())
		} else {
			// Session updated successfull
			status = "success"
		}

	} else {
		// Email not found
		status = "error"
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(status))
}
